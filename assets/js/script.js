//create array 
// use an array literal "[]"

// const array1 = ['eat', 'sleep'];
// console.log(array1);

//using new keyword 
const array2 = new Array('pray', 'play');
console.log(array2);

// empty array
const myList = [];

const newData = [
	{'task1': 'exercise'},
	[1,2,3],
	function hello(){
		console.log('hi!')
	}
];

console.log(newData);


console.log('=== mini activity ====');

let placesToGo = ['sagada', 'siargao', 'cebu', 'bacolod', 's.korea', 'japan', 'canada'];
console.log(placesToGo[0]);
console.log(placesToGo);
console.log(placesToGo[placesToGo.length-1]);

for (i=0; i<= placesToGo.length-1; i++ ) {
	console.log(placesToGo[i]);

}

placesToGo[3] = 'Pyramid of Giza';
console.log(placesToGo);
console.log(placesToGo[5]);

placesToGo[5] = 'Turkey';
console.log(placesToGo);

/*let dailyActivities = ['eat', 'work', 'pray', 'play'];
dailyActivities.push('exercise');
console.log(dailyActivities);

dailyActivities.unshift('sleep');
console.log(dailyActivities);

dailyActivities[2] = 'sing';
console.log(dailyActivities);

dailyActivities[7] = 'dance';
console.log(dailyActivities);*/


console.log('==mini activity #2 ===');

placesToGo.unshift('bacolod');
placesToGo[placesToGo.length-1] = 'DLNHS';
console.log(placesToGo);
console.log(placesToGo[0]);
console.log(placesToGo[placesToGo.length-1]);

let array3 = [];
console.log(array3[0]);
array3[0] = 'Lisa';
console.log(array3[0]);
array3[array3.length] = 'Rhaenys Targaryen';
console.log(array3);

// Array Methods
	// Manipulate Arrays with pre-determined JS Functions
	// Mutators - arrays methods usually change the original array

//without method
let blackPink = ['Lisa', 'Jennie', 'Jisoo'];
blackPink[blackPink.length] = 'Rose';
console.log(blackPink);

// with methods
// push add element at the end of array

blackPink.push('somi');
console.log(blackPink);

// unshift - add element to the first slot of array 
blackPink.unshift('Teddy');
console.log(blackPink);

// .pop() == to delete/remove an element at the end of array

blackPink.pop();
console.log(blackPink);

// it can also return the item removed
// console.log(blackPink.pop());
// console.log(blackPink);

let removed = blackPink.pop();
console.log(blackPink);
console.log(removed);

let removedShift = blackPink.shift();
console.log(blackPink);
console.log(removedShift);

console.log('== mini activity #3 ==');

let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];

array1.shift();
console.log(array1);

array1.pop();
console.log(array1);

array1.unshift('George');
console.log(array1);

array1.push('Michael');
console.log(array1);


// .sort == treating items as text/words and arrange it in order

array1.sort();
console.log(array1);

let numArray = [4,50,21,63,71,3,83];
numArray.sort((a,b) => (b-a)); // to sort in descending order 
console.log(numArray);

numArray.sort((a,b) => (a-b)); // to sort in ascending order / short hand to write a function
console.log(numArray);


//long method

numArray.sort(function(a,b) {
	return a-b
})
console.log(numArray);

let arrayStr = ['Marie', 'Zen', 'Jaime', 'Elaine'];
arrayStr.sort();
console.log(arrayStr);

// .reverse == sort in reverse order
arrayStr.reverse();
console.log(arrayStr);

let beatles = ['George', 'John', 'Paul', 'Ringo'];

let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'kobe', 'Shaq'];

//splice == allows to remove or add elements from/to given index syntax == array.splice(startingindex, #oftitemsToDelete, elementToAdd)

lakersPlayers.splice(0,0,'Caruso');
console.log(lakersPlayers);

lakersPlayers.splice(0,1);
console.log(lakersPlayers);

lakersPlayers.splice(0,3);
console.log(lakersPlayers);

lakersPlayers.splice(1,1);
console.log(lakersPlayers);

lakersPlayers.splice(1,0, 'Gasol', 'Fisher');
console.log(lakersPlayers);

// non-mutator methods == will not change the original 
	//slice== allows to get a portion of the original array and return a new array with the items seleccted from the original syntax: slice(startIndex, endIndex);

let compBrands = ['IBM', 'HP', 'Apple', 'MSI'];
compBrands.splice('Compaq', 'Toshiba', 'Acer');
console.log(compBrands);

let newBrands = compBrands.slice(1,3);
console.log(compBrands);
console.log(newBrands);

let fonts = ['Times New Roman', 'Comic Sans MS', 'impact', 'corsiva', 'arial', 'arial black'];
console.log(fonts);
let newFonts = fonts.slice(1,4);
console.log(newFonts);

newFonts = fonts.slice(1,2);
console.log(newFonts);

newFonts = fonts.slice(2);
console.log(newFonts);

newFonts = fonts.slice(0);
console.log(newFonts);

newFonts = fonts.slice(0).reverse();
console.log(newFonts);

console.log('=== mini activity #4 ===');

let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];

let microsoft = videoGame.slice(3);
console.log(microsoft);

let nintendo = videoGame.slice(2,4);
console.log(nintendo);

// .toString() - convert the array into a single value as a string but each item will be separated by a comma
// syntax: array.toString();

let sentence = ['I', 'like', 'Javascript', '.', 'It', 'is', 'fun', '.'];
console.log(sentence);
let sentenceStr = sentence.toString();
console.log(sentenceStr);

// .join() == convert the array into a single value as a string but the separator can be specified
//syntax array.join(separator);

let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'ArmyNavy'];

console.log(sentence2);
let sentenceStr2 = sentence2.join(' ');
console.log(sentenceStr2);

let sentenceStr3 = sentence2.join('');
console.log(sentenceStr3);


console.log('==== mini activity ====');

let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];


let name1Arr = charArr.slice(5,11);
let name1 =name1Arr.join('');
console.log(name1);


let name2Arr = charArr.slice(13,19);
let name2 = name2Arr.join('');
console.log(name2);


// concat() = combines two or more arrays without affecting the original
// syntax: array.concat(array1, array2);

let tasksFriday = ['drink HTML', 'eat Javascript'];
let tasksSaturday = ['inhale CSS', 'breath Bootstrap'];
let tasksSunday = ['Get Git', 'Be Node'];

let weekendTasks = tasksFriday.concat(tasksSaturday,tasksSunday);
console.log(weekendTasks);

// accessors
  // methods that allow us to access our array.
  // indexOf() = finds the index of the given elemet/item when it is first found from the left. 

 let batch131 = [
 		'Paolo',
 		'Jamir',
 		'Jed',
 		'Ronel',
 		'Rommel',
 		'Jayson'
 ];

 console.log(batch131.indexOf('Jed'));

 //LastIndexOf() = finds the index of the given element/item when it is last found from the right.

 console.log(batch131.lastIndexOf('Jamir'));
 console.log(batch131.lastIndexOf('Jayson'));

console.log('==== mini activity ====');

let carBrands = [
			'BMW',
			'Dodge',
			'Maserati',
			'Porsche',
			'Chevrolet',
			'Ferrari',
			'GMC',
			'Porsche',
			'Mitsubhisi',
			'Toyota',
			'Volkswagen',
			'BMW'
		];

function firstCar(first){
	console.log(carBrands.indexOf(first));
	

}
firstCar('Porsche');


function lastCar(last){
	console.log(carBrands.lastIndexOf(last));
}

lastCar('Porsche');

//iterator methods 
 //these methods iterate over the items in an array much like loop. however, there are also allow to not only iterate over items but also additional instructions.

 let avengers = [
 	'Hulk',
 	'Black Widow',
 	'Hawkeye',
 	'Spider-man',
 	'Iron Man',
 	'Captain America'
 ];

 // forEach() = used only in arrays, allows to iterate over each type in an array and even add instruction per iteration.
 //Anonymous function within forEach will be receive each and every item in an array.

 avengers.forEach(function(avenger){
 	console.log(avenger);
 });

 let marvelHeroes = [
 		'Moon Knight',
 		'Jessica Jones',
 		'Deadpool',
 		'Cyclops'
 ];

 marvelHeroes.forEach(function(hero){
 	if (hero !== 'Cyclops' && hero !== 'Deadpool' );
 		avengers.push(hero);
 });
 console.log(avengers);

 //map() = similar to forEach however it returns new array

let number = [25, 50, 30, 10, 5];
let mappedNumbers = number.map(function(number){
 		console.log(number);
 		return number*5
 });
console.log(mappedNumbers);

// every() = iterates overall the items and checks if all the elements passes a given condition kpag nakita si false, break

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18;

});
	console.log(checkAllAdult);


// some() = iterates overall items and checks if even atleast one of the items in the array passes the condition. Same as every() but it will return boolean. kapag nakita si true, break.

let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
});

console.log(checkForPassing);

// filter() creates a new array that contains elemnents which passed a given conditon 

let numArray2 = [500, 12, 120, 60, 6, 30];

let divisibleBy5 = numArray2.filter(function(number){
	return number % 5 ===0;
});
console.log(divisibleBy5);

// find() = iterate overall items in our array but only returns the item that will satisfy the given condition 

let registeredUsername = ['pedro101', 'mikeyTheKing2000',
'superPhoenix', 'sheWhoCode', 'wanCutie'];

// let foundUser = registeredUsername.find(function(username){
// 	console.log(username);
// 	return username === "mikeyTheKing2000";
// });

// console.log(foundUser);

// includes() = returns a boolean true if it finds a matching item in the array. case sensitive.

let registeredEmail = [
	'johnnyPhoenix1991@gmail.com',
	'michaelKing@gmail.com',
	'pedro_himself@yahoo.com',
	'sheJoneSmith@gmail.com'
];

let doesEmailExist = registeredEmail.includes('michaelKing1@gmail.com');
console.log(doesEmailExist);

console.log('=== mini activity ====');
console.log('=== mini activity ====');
console.log('=== mini activity ====');
console.log('=== mini activity ====');



let foundUser = registeredUsername.find(function(usern){
	return usern === 'wanCutie';
});
console.log(foundUser);

function alertEmail(email){
		let exist = registeredEmail.includes(email);
		if (exist === true) {
			alert('Email Already Exist.');
		} else {
			alert('Email is available, proceed to registration');
		}
}

alertEmail('pedro_himself@gmail.com');

